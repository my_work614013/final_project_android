//package com.example.inventory.data
//
//import androidx.room.*
//import kotlinx.coroutines.flow.Flow
//
//@Dao
//interface ItemDao2 {
//    @Insert(onConflict = OnConflictStrategy.IGNORE)
//    suspend fun insert(item2: Item2)
//
//    @Update
//    suspend fun update(item2: Item2)
//
//    @Delete
//    suspend fun delete(item2: Item2)
//
//    @Query("SELECT * from item2 WHERE id = :id")
//    fun getItem(id: Int): Flow<Item2>
//
//    @Query("SELECT * from item2 ORDER BY name ASC")
//    fun getItems(): Flow<List<Item2>>
//}