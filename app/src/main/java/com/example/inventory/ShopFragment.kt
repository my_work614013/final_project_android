package com.example.inventory
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.inventory.adapter.ShopListAdapter
import com.example.inventory.data.Item
import com.example.inventory.databinding.FragmentShopBinding

class ShopFragment : Fragment() {
    private val viewModel: InventoryViewModel by activityViewModels {
        InventoryViewModelFactory(
            (activity?.application as InventoryApplication).database.itemDao()
        )
    }

    private val navigationArgs: ItemDetailFragmentArgs by navArgs()

    lateinit var item: Item
    private var _binding: FragmentShopBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentShopBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        if(viewModel.count >= 1){
            super.onViewCreated(view, savedInstanceState)
            binding.recyclerView2.layoutManager = LinearLayoutManager(this.context)
            val adapter = ShopListAdapter {
                val action = ShopFragmentDirections.actionShopFragmentToShopListFragment(it.id)
                this.findNavController().navigate(action)
            }
            binding.recyclerView2.adapter = adapter
            viewModel.allItems.observe(this.viewLifecycleOwner) { items ->
                items.let {
                    if (viewModel.count >= 1){
                        adapter.submitList(it)
                    }
                }
            }
        }else if(viewModel.count == 0) {
            super.onCreate(null)
        }
        binding.buyItem.setOnClickListener {
            viewModel.count = 0
            val action = ShopFragmentDirections.actionShopFragmentToItemListFragment()
            findNavController().navigate(action)
        }
    }
}


