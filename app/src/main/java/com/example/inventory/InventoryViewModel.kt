package com.example.inventory

import android.icu.util.CurrencyAmount
import android.nfc.Tag
import androidx.lifecycle.*
import com.example.inventory.data.Item
import com.example.inventory.data.ItemDao

import kotlinx.coroutines.launch
import java.time.temporal.TemporalAmount
import kotlin.math.log


class InventoryViewModel(private val itemDao: ItemDao) : ViewModel() {

    var allItems: LiveData<List<Item>> = itemDao.getItems().asLiveData()
    var count = 0
    var buycount = 0

    private fun insertItem(item: Item) {
        viewModelScope.launch {
            itemDao.insert(item)
        }
    }

    private fun getNewItemEntry(itemName: String, itemPrice: String, itemCount: String): Item {
        return Item(
            itemName = itemName,
            itemPrice = itemPrice.toDouble(),
            quantityInStock = itemCount.toInt(),
        )
    }

    private fun getUpdatedItemEntry(
        itemId: Int,
        itemName: String,
        itemPrice: String,
        itemCount: String,
    ): Item {
        return Item(
            id = itemId,
            itemName = itemName,
            itemPrice = itemPrice.toDouble(),
            quantityInStock = itemCount.toInt(),
        )
    }

    fun addNewItem(itemName: String, itemPrice: String, itemCount: String) {
        val newItem = getNewItemEntry(itemName, itemPrice, itemCount)
        insertItem(newItem)
    }

    fun updateItem(
        itemId: Int,
        itemName: String,
        itemPrice: String,
        itemCount: String,
    ) {
        val updatedItem = getUpdatedItemEntry(itemId, itemName, itemPrice, itemCount)
        updateItem(updatedItem)
    }

    fun isEntryValid(itemName: String, itemPrice: String, itemCount: String): Boolean {
        if (itemName.isBlank() || itemPrice.isBlank() || itemCount.isBlank()) {
            return false
        }
        return true
    }

    fun retrieveItem(id: Int): LiveData<Item> {
        return itemDao.getItem(id).asLiveData()
    }

     private fun updateItem(item: Item) {
        viewModelScope.launch {
            itemDao.update(item)
        }
    }

    fun sellItem(item: Item) {
        if (item.quantityInStock > 0) {
            count+=1
            val newItem = item.copy(quantityInStock = item.quantityInStock - 1,
                itemAmount = item.itemAmount + 1,
                itemTotal = ((item.itemPrice * item.itemAmount) + item.itemPrice)
            )
            updateItem(newItem)
        }
    }

    fun plusAmount(item: Item) {
        if (item.itemAmount >= 1) {
            val plus = item.copy(itemAmount = item.itemAmount+1, itemTotal = item.itemTotal + item.itemPrice)
            updateItem(plus)
        }
    }

    fun minusAmount(item: Item) {
        if (item.itemAmount > 0) {
            val minus = item.copy(itemAmount = item.itemAmount-1, itemTotal = item.itemTotal - item.itemPrice)
            updateItem(minus)
        }
    }

    fun isStockAvailable(item: Item): Boolean {
        return (item.quantityInStock > 0)
    }

    fun deleteItem(item: Item) {
        viewModelScope.launch {
            itemDao.delete(item)
        }
    }
}

class InventoryViewModelFactory(private val itemDao: ItemDao) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(InventoryViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return InventoryViewModel(itemDao) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

}